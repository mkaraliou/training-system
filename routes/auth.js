const passport = require('passport');
const express = require('express');
const router = express.Router();

router.get('/bitbucket/login', passport.authenticate('bitbucket'));

router.get('/bitbucket/callback', passport.authenticate('bitbucket', { failureRedirect: '/' }),
  function (req, res) {
    res.redirect('/users');
  });

router.get('/bitbucket/logout', (req, res) => {
  req.logout();
  res.redirect("/");
});

module.exports = router;