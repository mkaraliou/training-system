const express = require('express');
const router = express.Router();
const routerUtils = require('../utils/router')

router.get('/', routerUtils.redirectIfNotAuthenticated('/auth/bitbucket/login'), (req, res) => {
    res.render('user', { title: 'User profile', user: req.user});
});

module.exports = router;
