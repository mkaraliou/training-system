const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
    username: String,
    displayName: String,
    avatar: String,
    email: String
});

module.exports = mongoose.model("Student", studentSchema);