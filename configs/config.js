const path = require('path');
const nconf = module.exports = require('nconf');

function checkConfig(setting) {
    if (!nconf.get(setting)) {
      throw new Error(`You must set ${setting} as an environment variable or in config.json!`);
    }
}

nconf
    .argv()
    .env([
      'PORT',
      'BB_OAUTH_CLIENT_ID',
      'BB_OAUTH_CLIENT_SECRET',
      'BB_OAUTH_CALLBACK',
      'SESSION_SECRET',
      'MONGO_URL',
      'REPO'
    ])
    .file({ file: path.join(__dirname, '../config.json') })
    .defaults({
      PORT: 3000,
      BB_OAUTH2_CLIENT_ID: '',
      BB_OAUTH2_CLIENT_SECRET: '',
      BB_OAUTH2_CALLBACK: '',
      SESSION_SECRET: '',
      MONGO_URL: '',
      REPO: ''
    });

checkConfig('PORT');
checkConfig('BB_OAUTH_CLIENT_ID');
checkConfig('BB_OAUTH_CLIENT_SECRET');
checkConfig('BB_OAUTH_CALLBACK');
checkConfig('SESSION_SECRET');
checkConfig('MONGO_URL');
checkConfig('REPO');