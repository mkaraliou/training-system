const mongoose = require('mongoose');

module.exports = (config) => {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.get("MONGO_URL"), {useMongoClient: true});
}