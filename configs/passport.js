const session = require('express-session');
const url = require('url');
const BitbucketStrategy = require('passport-bitbucket-oauth2').Strategy;
const bitbucket = require('../utils/bitbucket');
const Student = require('../models/student');

module.exports = (app, config, passport) => {
  //TODO: take a look on session security
  app.use(session({
    secret: config.get('SESSION_SECRET'),
    saveUninitialized: false,
    resave: false
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  passport.use(new BitbucketStrategy({
    clientID: config.get('BB_OAUTH_CLIENT_ID'),
    clientSecret: config.get('BB_OAUTH_CLIENT_SECRET'),
    callbackURL: config.get('BB_OAUTH_CALLBACK')
  },
    (accessToken, refreshToken, profile, done) => {
      Student.findOne({ username: profile.username })
        .then((mongoStudent) => {
          return !mongoStudent
            ? bitbucket.getUserFromOauthAndRegister(profile, accessToken)
            : mongoStudent;
        })
        .then((mongoStudent) => {
          done(null, {
            accessToken: accessToken,
            refreshToken: refreshToken,
            student: mongoStudent
          });
        })
        .catch((err) => {
          done(err);
        });
    }
  ));

  passport.serializeUser((studentWithToken, done) => {
    done(null, {
      username: studentWithToken.student.username,
      displayName: studentWithToken.student.displayName,
      avatar: studentWithToken.student.avatar,
      email: studentWithToken.student.email,
      accessToken: studentWithToken.accessToken,
      refreshToken: studentWithToken.refreshToken
    });
  });

  passport.deserializeUser((obj, done) => {
    done(null, obj);
  });
}