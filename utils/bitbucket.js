const rq = require('request-promise-native');
const Promise = require('Promise');
const config = require('../configs/config');
const Student = require('../models/student');

module.exports = {
  getUserFromOauthAndRegister: (oauth, accessToken) => {
    const student = {
      username: oauth.username,
      displayName: oauth.displayName,
      avatar: oauth._json.links.avatar.href
    };
    return rq.get("https://api.bitbucket.org/2.0/user/emails?access_token=" + accessToken, { json: true })
      .then((resp) => {
        const emails = resp.values;
        for (var i = 0; i < emails.length; ++i) {
          const email = emails[i];
          if (email.is_primary) {
            student.email = email.email;
            return new Promise((resolve) => {
              resolve(student);
            });
          }
        }
        throw new Error("Unable to find primary email");
      })
      .then((student) => {
        return Student.create(new Student(student));
      })
  },
  verifyRepository: (user) => {
    const filter = user.username + "/" + config.get('REPO');
    return rq.get("https://api.bitbucket.org/2.0/repositories/GreenBanan/aaaa/forks?q=full_name=\"" + filter + "\"&access_token=" + user.accessToken, {json: true})
    .then ((resp) => {
      return new Promise((resolve) => resolve(resp.size !== 0));
    });
  }
}