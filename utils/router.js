module.exports = {
    redirectIfNotAuthenticated: (path) => {
        return (req, res, next) => {
            if (req.isAuthenticated()) {
                return next();
            }
            res.redirect(path);
        }
    }
}